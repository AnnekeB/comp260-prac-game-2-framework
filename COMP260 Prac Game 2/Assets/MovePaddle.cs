﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class MovePaddle : MonoBehaviour {
	private new Rigidbody rigidbody;
	public float speed = 20f;
	public bool UseKeyboard;
	public Vector3 posKeyboard;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
		rigidbody.useGravity = false;
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log ("Time = " + Time.fixedTime);
	}

	void FixedUpdate(){

		if (!UseKeyboard){
			Debug.Log ("Fixed Time = " + Time.fixedTime);
			Vector3 pos = GetMousePosition ();
			Vector3 dir = pos - rigidbody.position;
			Vector3 vel = dir.normalized * speed;

			//avoid overshoot
			float move = speed * Time.fixedDeltaTime;
			float distToTarget = dir.magnitude;
			if (move > distToTarget) {
				vel = vel * distToTarget / move;
			}
			rigidbody.velocity = vel;
		}

		if (UseKeyboard){
			float h = Input.GetAxis ("Horizontal");
			float v = Input.GetAxis ("Vertical");

			posKeyboard = new Vector3(h, 0, v);

			rigidbody.velocity = (posKeyboard * speed);
		}

	}

	private Vector3 GetMousePosition (){
		//create a ray from camera
		//passing through the mouse position
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

		//find where the ray intersects the XZ plane
		Plane plane = new Plane(Vector3.up, Vector3.zero);
		float distance = 0;
		plane.Raycast (ray, out distance);
		return ray.GetPoint (distance);
			
	}

	void OnDrawGizmo (){
		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePosition ();
		Gizmos.DrawLine (Camera.main.transform.position, pos);

	}
}
